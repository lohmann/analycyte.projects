
<!-- README.md is generated from README.Rmd. Please edit that file -->

# analycyte.projects

[initial git](https://github.com/ClevelandClinicQHS/projects)

### Authors

Nikolas I. Krieger, M.S.;<sup>1</sup> Adam T. Perzynski,
Ph.D.;<sup>2</sup> and Jarrod E. Dalton, Ph.D.<sup>1,3</sup>

<sup>1</sup> Department of Quantitative Health Sciences, Lerner Research
Institute, Cleveland Clinic, 9500 Euclid Avenue (JJN3), Cleveland, OH,
44195  
<sup>2</sup> Center for Healthcare Research and Policy, Case Western
Reserve University at MetroHealth, 2500 MetroHealth Drive, Cleveland, OH
44109  
<sup>3</sup> Cleveland Clinic Lerner College of Medicine, Case Western
Reserve University

Acknowledgements:  
The authors of this package acknowledge the support provided by members
of the Northeast Ohio Cohort for Atherosclerotic Risk Estimation
(NEOCARE) investigative team: Claudia Coulton, Douglas Gunzler, Darcy
Freedman, Neal Dawson, Michael Rothberg, David Zidar, David Kaelber,
Douglas Einstadter, Alex Milinovich, Monica Webb Hooper, Kristen
Hassmiller-Lich, Ye Tian (Devin), Kristen Berg, and Sandy Andrukat.

Funding:  
This work was supported by The National Institute on Aging of the
National Institutes of Health under award number R01AG055480. The
content is solely the responsibility of the authors and does not
necessarily represent the official views of the National Institutes of
Health.

### Installation

You can install `analycyte.projects` with:

``` r
remotes::install_git("https://gitcrcm.marseille.inserm.fr/lohmann/analycyte.projects")
```

## Introduction

`analycyte.projects` uses the `projects`’ infrastructure to store R
shiny users interaction with app and template for its analysis reports.

The goal of the `projects` R package is to provide a set of tools that
support an efficient project management workflow for statisticians and
data scientists who perform reproducible research within team science
environments. The `projects` package is built upon some existing tools
for reproducible research, particularly RStudio, the R integrated
development environment in which it dwells, and R Markdown, the file
structure that allows users to assemble datasets, to perform analyses
and to write manuscripts in a single file. The `projects` package is
oriented towards efficient and reproducible academic research manuscript
development and incorporates protocol and analysis templates based on
widely-accepted reporting guidelines (viz., CONSORT and STROBE). When
used on a shared file system (e.g., a server), the `projects` package
provides infrastructure for collaborative research: multiple researchers
can work on the same project and keep track of its progress without
having to request updates.

The primary features of the projects R package are the following:

- **Relational database containing details of projects, its granular
  tasks, and project coauthors and their affiliations, so that author
  details generally need to be entered only once;**
- **Tools for editing metadata associated with projects, tasks, authors
  and affiliations;**
- **Automated file structure supporting reproducible research
  workflow;**
- **Organization and management functionality, including the ability to
  group, archive and delete projects.**

still available from `projects`:

- Report templates that automatically generate title page headers,
  including a numbered author list and corresponding affiliations;
- Full RStudio integration via R Markdown, including customizable
  styling via cascading style sheets (CSS); and
- Customization, including the ability to add and to edit templates for
  protocols and reports, and the ability to change default project
  directory and file structures

At its outset, the `projects` package creates a folder called
*/projects* in a user-specified location. This directory will contain
all research projects created and maintained by the user. The
*/projects* folder will also contain a relational database of the
research projects and the persons who contribute to them. The database
allows for users to input important metadata about the projects and
authors, including stage of research and contact information. Once this
higher-level folder is created, users run R functions to create
projects, each of which is given its own folder. New project folders
automatically include aptly named subfolders and templates in order to
guide the project workflow (e.g., a “data” subfolder; a “datawork” R
Markdown template).

## Conceptual Framework

Reproducibility in research is the focus of the much debated replication
crisis and is therefore an increasingly central goal of the contemporary
scientific process. In addition to a final report of study results,
reproducible research processes include the entire workflow that the
researchers used to generate those results. Actively maintaining and
archiving this workflow is important to the evaluation and validation of
the research. If other researchers can follow the same workflow to
achieve the same results, it corroborates the results as scientific
knowledge. When results are not produced by the same workflow, however,
scientific knowledge is still advanced, as the workflow is shown not to
yield generalizable results. (Baker 2016)

![](man/README/dbb_first_try.svg)

## The `analycyte.projects` tables

### Projects Table Columns

- `id` – an identification number, specifically an integer, unique among
  the other projects. This number can be used whenever needing to
  identify this project within `analycyte.projects` package functions.
- `title` – the title of the project. A nonambiguous substring of
  `title` (i.e., a substring that does not match any other project) can
  be used whenever needing to identify this project within
  `analycyte.projects` package functions.
- `short_title` – an optional unique nickname for the project. A
  nonambiguous substring of `short_title` (i.e., a substring that does
  not match any other project) can be used whenever needing to identify
  this project within `analycyte.projects` package functions. This is
  useful if users cannot remember the long, formal project `title` nor
  the project `id`.
- `current_owner` – the `id` of the author who is responsible for taking
  action in order that work on the project may proceed further.
- `status` – a short description of the status of the project. For
  example, it may elaborate on the value of `current_owner` and/or
  `stage`.
- `impact` — a number depicting the estimated impact that this project
  will have.
- `deadline_type` – a simple description of the meaning of the date
  contained in the next field, `deadline`.
- `deadline` – a date indicating some kind of deadline whose meaning is
  described in the previous field, `deadline_type`.
- `stage` – one of seven predefined stages of project development that
  the project is currently in:
  - `c("0: idea",`
  -     `"1: design",`
  -     `"2: data collection",`
  -     `"3: analysis",`
  -     `"4: manuscript",`
  -     `"5: under review",`
  -     `"6: accepted")`
- `path` – the full file path where the project folder is located.
- `corresp_auth` – the `id` of the author who should be contacted for
  any correspondence relating to the project. This author’s name will be
  especially marked on automatically generated title pages for this
  project, and his or her contact information will be especially
  displayed there as well in a “Corresponding Author” section.
- `creator` – the `id` of the author who initially created the project,
  or the value of `Sys.info()["user"]` if the author who ran
  `new_project()` did not enter a value.

### Authors Table

- `id` – an identification number, specifically an integer, unique among
  the other authors. This number can be used whenever needing to
  identify this author within `analycyte.projects` package functions.
- `given_names` – the given name or names of the author. A nonambiguous
  substring of `given_names` (i.e., a substring that does not match any
  other author) can be used whenever needing to identify this author
  within `analycyte.projects` package functions. This is included in the
  automatically generated title pages of the projects associated with
  this author.
- `last_name` – the last name or names of the author. A nonambiguous
  substring of `last_name` (i.e., a substring that does not match any
  other author) can be used whenever needing to identify this author
  within `analycyte.projects` package functions. This is included after
  `given_names` in the automatically generated title pages of the
  projects associated with this author.
- `title` – the job title of the author.
- `degree` – the abbreviation(s) of the author’s academic degree(s).
  This is included after `last_name` in the automatically generated
  title pages of the projects associated with this author.
- `email` – the email address of the author. This is included in the
  “Corresponding Author” section of the automatically generated title
  pages of projects whose `corresp_auth` field contains this author.
- `phone` – the phone number of the author. This is included in the
  “Corresponding Author” section of the automatically generated title
  pages of projects whose `corresp_auth` field contains this author.

### Affiliations Table

- `id` – an identification number, specifically an integer, unique among
  the other affiliations. This number can be used whenever needing to
  identify this affiliation within `analycyte.projects` package
  functions.
- `department_name` – the department name of the affiliation. A
  nonambiguous substring of `department_name` (i.e., a substring that
  does not match any other affiliation) can be used whenever needing to
  identify this affiliation within `analycyte.projects` package
  functions. This is included in the affiliations section of the
  automatically generated title page of projects associated with authors
  with this affiliation.
- `institution_name` – the name of the overall institution of the
  affiliation. A nonambiguous substring of `institution_name` (i.e., a
  substring that does not match any other affiliation) can be used
  whenever needing to identify this affiliation within
  `analycyte.projects` package functions. This is included after
  `department_name` in the affiliations section of the automatically
  generated title page of projects associated with authors with this
  affiliation.
- `address` – the address of the affiliation. This is included after
  `institution_name` in the affiliations section of the automatically
  generated title page of projects associated with authors with this
  affiliation. It is also included in the “Corresponding Author” section
  of the title page when a project’s corresponding author has this
  affiliation as his or her primary (i.e., first) affiliation (see the
  **Internal Tables** section).

### Tasks Table

- `PID` — the `id` of the project that the task is associated with (see
  above).
- `TID` — the identification number of the task, also representing the
  order of priority within its associated project. For each project, its
  tasks’ `TID`s are always sequential positive integers starting with 1
- `done` — a binary (1/0) indicator of the task’s completion status.
- `effort` — a numeric value indicating the level of effort that it will
  require to complete the task
- `timing` — a numeric value indicating the nature of the timing
  associated with the completion of the task
- `lead` — the `id` of the author who will take the lead in completing
  the task.
- `status` — the status of the task.

### Internal Tables

In keeping with relational database theory, there are two *.rds* files
that keep track of the many-to-many relationships between projects and
authors and between authors and affiliations. Each has two columns,
`AuID`/`PID` and `AfID`/`AuID`, that contain the `id` numbers of these
items. Each row of this table describes an association. Furthermore, the
`analycyte.projects` package keeps track of the order in which these
associations appear so that the automatically generated title pages list
authors and affiliations in the correct order. Users are able to run
functions to reorder these associations as needed.

### Steps Table

- `SID` – the identification number of the step, also representing the
  order of priority within its associated project. For each project, its
  steps’ `SID`s are always sequential positive integers starting with 1

- `PID` – the `id` of the project that the step is associated with (see
  above).

- `hash_tag` – Automatically generated and uniq hash base on time to
  store the step output in a folder.

- `label` – label of the module used in app; ie: `mod_import_rds`.

- `comment` – comments or additional information related to the step.

- `TID` – the identification number of the task, also representing the
  order of priority within its associated project. For each project, its
  tasks’ `TID`s are always sequential positive integers starting with 1

- `input_hash_tag` – uniq hash of the folder of reference for the
  related step.

- `output_type` – An integer/string special type (`steps_filetype`)
  representing the type of input/output generated by the step. The
  possible values are:

  - `0_none`: No output required.
  - `1_fcs`: Flow Cytometry Standard (FCS) file type.
  - `2_fb`: FCS bunch, FCS, pheno, panels.
  - `3_sce`: SingleCellExperiment class from GenomicRanges type.
  - `4_files`: other file types, including Excel or csv files.
  - `5_fe`: Feature Set class,SummarizedExperiment class from
    GenomicRanges type.
  - `6_html`: HTML output.

- `path` – the full file path where the project folder is located.

- `execution_start` – A datetime field marking the start time of the
  step’s execution.

- `execution_end` – A datetime field marking the end time of the step’s
  execution.

- `status` – a short description of the status of the step:

  `not_started`, `in_progress`, `completed_successfully`,
  `completed_with_errors`, `failed`, `paused` and `canceled`

- `error_message` – A text field to record any error messages
  encountered during the step’s execution.

- `transf_locked` – A boolean field indicating whether the step is
  locked for transformation (input or output).

- `has_clusters` – A boolean field indicating whether the step involves
  clustering operations (input or output).

- `has_reddims` – A boolean field indicating whether the step includes
  dimensionality reduction operations (input or output).

### Tools Table

- `ToID` – the identification number of the tool. `ToID` are always
  sequential positive integers starting with 1
- `description` – Information related to the relative module/tool
- `prerequite_type` – special type (`steps_filetype`) representing the
  type of input of the module (see above, step table)
- `output_type` – special type (`steps_filetype`) representing the type
  of output of the module (see above, step table)
- `R_filename` – R file containing module code for debugging purpose.
- `yaml_filename` –
- `run_on_server` – A Boolean field indicating whether the module can
  run on the IFB server

### *Files Type*

- *`FTID` – int \[pk, increment\]*
- *`type_name` – varchar(255) \[not null, unique\]*
- *`description` – text*

## Project File Structure

Users create individual project folders with the function
`new_project()`. By default, the name of each project folder is of the
form p*XXXX*, where *XXXX* is the project’s `id` padded with 0s on the
left side. Its contents are copied from a template project folder within
the *.templates* directory in the main */projects* folder.

The included subfolders serve to organize the project, while the *.qmd*
files are templates for the cytometry analysis.

The default project folder template is structured as follows:

    /pXXXX
    ├── progs
    │   ├── DA.qmd
    │   ├── DA_extended.qmd
    │   ├── DS.qmd
    │   ├── Parent.qmd
    │   ├── QC.qmd
    │   ├── _extensions
    │   │   └── crcm
    │   │       └── ressources
    │   └── www
    └── pXXXX.Rproj

# Demonstration

Please refer to the demonstration on the original `projects` package, as
the `analycyte.projects` package does not have to be launched by the
user on the command line.

# Conclusions

`analycyte.projects` adapts the `projects` package to the `analycyte`
environment. The `projects` package provides a comprehensive set of
tools for reproducible team science workflows. Efficiency in project
management, including manuscript development, is facilitated by an
internal database that keeps record of project details as well as team
members’ affiliations and contact information. For manuscripts, title
pages are automatically generated from this database, and a selection of
manuscript outlines compliant with reporting guidelines are available in
R Markdown format. We believe that the `projects` package may be useful
for teams that manage multiple collaborative research projects in
various stages of development.

# References

<div id="refs" class="references csl-bib-body hanging-indent">

<div id="ref-baker20161" class="csl-entry">

Baker, Monya. 2016. “1,500 Scientists Lift the Lid on Reproducibility.”
*Nature News* 533 (7604): 452.

</div>

</div>
