#' \code{steps_filetype} vector
#'
#' Objects of this class are merely a character string containing a file type.
#'
#' A \code{steps_filetype} object is either a missing value (\code{NA}) or one
#' of:
#'
#' \code{0: none}\cr \code{1: fcs}\cr \code{2: fb}\cr \code{3: sce}\cr \code{5: fe}\cr
#' \code{4: files}\cr \code{6: html}
#'
#' \code{steps_filetype()} validates and coerces a vector of the above integers or strings to a \code{steps_filetype} S3 vector.
#'
#' @section Numeric coercion methods: \code{\link{as.integer}()},
#'   \code{\link{as.double}()}, and \code{\link{as.numeric}()} return the filetype
#'   number of the \code{steps_filetype} object as an integer/double. The
#'   methods for the comparison and value matching functions described below
#'   make use of these numeric coercion methods. Users desiring to apply value
#'   matching functions other than the ones described below may similarly take
#'   advantage of these.
#'
#' @section Comparison and value matching methods: Methods for the
#'   \link{Comparison} operators as well as \code{\link{match}()} and
#'   \code{\link{\%in\%}} enable users to test equality and to value match among
#'   \code{steps_filetype} objects and as well as between \code{steps_filetype}
#'   objects and unclassed numbers/characters. When comparing or value matching
#'   against a numeric vector, the \code{steps_filetype} object is first coerced
#'   to an integer with the \code{as.integer()} method described above. When
#'   testing or value matching against a character vector, the character vector
#'   is validated against the list of project filetypes enumerated above.
#' @param ... Additional arguments to be passed to other methods.
#' @param x For \code{steps_filetype()}, an integer or character vector. For
#'
#'   For \code{\link{match}()} and \code{\link{\%in\%}}, an integer, a character
#'   string, or a \code{steps_filetype} object. See \code{\link{match}()} and
#'   \strong{Comparison and value matching methods} below.
#'
#' @param table An integer number, a character string, or a
#'   \code{steps_filetype} object. See \code{\link{match}()} and
#'   \strong{Comparison and value matching methods} below.
#'
#' @param nomatch See \code{\link{match}()}.
#'
#' @param incomparables An integer number, a character string, or a
#'   \code{steps_filetype} object. See \code{\link{match}()}.
#'
#' @param e1 The first steps_filetype object in the comparison.
#' @param e2 The second steps_filetype object in the comparison.
#'
#' @param ties.method is "average"
#' @param decreasing boolean
#'
#' @return For \code{steps_filetype()}, an S3 vector of class
#'   \code{steps_filetype}.
#'
#' @seealso \code{\link{Ops}}; \code{\link[methods]{Methods_for_Nongenerics}}.
#'
#' @export
steps_filetype <- function(x = character()) {
  x <- as.character(x)
  validate_filetype(x)
}


new_steps_filetype <- function(x = character()) {
  vec_assert(x, character())
  new_vctr(x, class = "steps_filetype")
}


#' @rdname steps_filetype
#' @importFrom methods setClass
#' @export
setClass("steps_filetype")


#' Abbreviated type of steps_filetype
#'
#' This method returns an abbreviated string representation for the `steps_filetype` object, suitable for use in a column heading.
#' @param x An object of class `steps_filetype`.
#' @param ... ...
#' @return A character string representing the abbreviated type of `steps_filetype`.
#' @export
vec_ptype_abbr.steps_filetype <- function(x, ...) "prjft"


validate_filetype <- function(filetype, na.ok = TRUE, null.ok = TRUE, n = NULL) {
  if (is.null(filetype) && null.ok) {
    return(NULL)
  }

  choices <- c("0: none", "1: fcs", "2: fb", "3: sce", "4: files", "5: fe", "6: html")
  # choices <- eval(formals(new_step)$output_type)

  filetype <- trimws(tolower(as.character(filetype)))

  if (!rlang::is_atomic(filetype) || !is.null(n) && length(filetype) != n) {
    stop("\nfiletype must be coercible to a character vector of length ", n)
  }

  filetype <-
    vapply(
      filetype,
      function(filetype) {
        if (is.na(filetype)) {
          if (!na.ok) {
            stop("filetype must not be missing (NA)")
          }
        } else {
          match_attempt <- pmatch(filetype, choices)

          if (is.na(match_attempt)) {
            match_attempt <- pmatch(filetype, substr(choices, 4L, nchar(choices)))

            if (is.na(match_attempt)) {
              stop(
                "\nTo match a filetype, user input must either:\n\n",
                "- exactly match the integer\n",
                "- partially match the text\n\n",
                "of one of:\n",
                paste(choices, collapse = "\n"),
                "\n\n'", filetype, "' did not match."
              )
            }
          }

          filetype <- choices[match_attempt]
        }
      },
      FUN.VALUE = character(1L),
      USE.NAMES = FALSE
    )

  new_steps_filetype(filetype)
}



#' @rdname steps_filetype-vctrs
#' @method vec_ptype2 steps_filetype
#' @export
#' @export vec_ptype2.steps_filetype
vec_ptype2.steps_filetype <- function(x, y, ...) {
  UseMethod("vec_ptype2.steps_filetype", y)
}

#' @method vec_ptype2.steps_filetype default
#' @export
vec_ptype2.steps_filetype.default <- function(x, y, ...,
                                              x_arg = "x", y_arg = "y") {
  vec_default_ptype2(x, y, x_arg = x_arg, y_arg = y_arg)
}

#' @method vec_ptype2.steps_filetype steps_filetype
#' @export
vec_ptype2.steps_filetype.steps_filetype <- function(x, y, ...) {
  new_steps_filetype()
}

#' @method vec_ptype2.steps_filetype character
#' @export
vec_ptype2.steps_filetype.character <- function(x, y, ...) character()

#' @method vec_ptype2.character steps_filetype
#' @export
vec_ptype2.character.steps_filetype <- function(x, y, ...) character()

#' @method vec_cast steps_filetype
#' @export vec_cast.steps_filetype
#' @export
#' @rdname steps_filetype-vctrs
vec_cast.steps_filetype <- function(x, to, ...) {
  UseMethod("vec_cast.steps_filetype")
}

#' @method vec_cast.steps_filetype default
#' @export
vec_cast.steps_filetype.default <- function(x, to, ...) {
  vec_default_cast(x, to)
}

#' @method vec_cast.steps_filetype steps_filetype
#' @export
vec_cast.steps_filetype.steps_filetype <- function(x, to, ...) x

#' @method vec_cast.steps_filetype character
#' @export
vec_cast.steps_filetype.character <- function(x, to, ...) validate_filetype(x)

#' @method vec_cast.character steps_filetype
#' @export
vec_cast.character.steps_filetype <- function(x, to, ...) vec_data(x)

#' @method vec_cast.steps_filetype integer
#' @export
vec_cast.steps_filetype.integer <- function(x, ...) validate_filetype(x)

#' @method vec_cast.integer steps_filetype
#' @export
vec_cast.integer.steps_filetype <- function(x, ...) {
  as.integer(substr(vec_data(x), 1L, 1L))
}

#' @method vec_cast.double steps_filetype
#' @export
vec_cast.double.steps_filetype <- function(x, ...) {
  as.double(substr(vec_data(x), 1L, 1L))
}

#' @method vec_cast.steps_filetype double
#' @export
vec_cast.steps_filetype.double <- function(x, ...) validate_filetype(x)


#' @export
Ops.steps_filetype <- function(e1, e2) {
  get(.Generic)(
    vapply(validate_filetype(e1), as.integer, 0L),
    vapply(validate_filetype(e2), as.integer, 0L)
  )
}




# Generic methods for match() --------------------------------------------------

#' @rdname steps_filetype
#' @export
match.steps_filetype <- function(x,
                                 table,
                                 nomatch = NA_integer_,
                                 incomparables = NULL) {
  x <- validate_filetype(x)
  table <- validate_filetype(table)
  if (!is.null(incomparables)) {
    incomparables <- validate_filetype(incomparables)
  }

  base::match(x, table, nomatch, incomparables)
}

#' @include 01_set_generics.R
#' @rdname steps_filetype
#' @importFrom methods signature setMethod
#' @export
setMethod(
  "match",
  signature(x = "steps_filetype"),
  match.steps_filetype
)

#' @include 01_set_generics.R
#' @rdname steps_filetype
#' @importFrom methods signature setMethod
#' @export
setMethod(
  "match",
  signature(table = "steps_filetype"),
  match.steps_filetype
)

#' @include 01_set_generics.R
#' @rdname steps_filetype
#' @importFrom methods signature setMethod
#' @export
setMethod(
  "match",
  signature(x = "steps_filetype", table = "steps_filetype"),
  match.steps_filetype
)





# Generic methods for %in% -----------------------------------------------------

#' @rdname steps_filetype
#' @export
`%in%.steps_filetype` <- function(x, table) {
  match(x, table, nomatch = 0L) > 0L
}

#' @include 01_set_generics.R
#' @rdname steps_filetype
#' @importFrom methods setMethod signature
#' @export
setMethod(
  "%in%",
  signature("steps_filetype"),
  `%in%.steps_filetype`
)

#' Internal vctrs methods
#'
#' @import vctrs
#' @keywords internal
#' @name steps_filetype-vctrs
NULL



# Ordering method------
filetype_order <- c("none" = 0, "fcs" = 1, "fb" = 2, "sce" = 3, "files" = 4, "fe" = 5, "html" = 6)

#' @include 01_set_generics.R
#' @rdname steps_filetype
#' @export
Ops.steps_filetype <- function(e1, e2) {
  # Extract the integer values based on your ordering
  e1_int <- as.integer(substring(vec_data(validate_filetype(e1)), 1, 1))
  e2_int <- as.integer(substring(vec_data(validate_filetype(e2)), 1, 1))

  # Perform the operation based on the extracted integers
  switch(.Generic,
    "<" = e1_int < e2_int,
    "<=" = e1_int <= e2_int,
    ">" = e1_int > e2_int,
    ">=" = e1_int >= e2_int,
    "==" = e1_int == e2_int,
    "!=" = e1_int != e2_int,
    stop(paste("Unsupported operator:", .Generic))
  )
}

#' @include 01_set_generics.R
#' @rdname steps_filetype
#' @export
get_filetype_numeric <- function(e1) {
  sapply(e1, function(ft) {
    if (is.na(ft)) {
      return(NA_real_)
    }
    as.numeric(substring(ft, 1, 1))
  })
}

#' @include 01_set_generics.R
#' @rdname steps_filetype
#' @export
sort.steps_filetype <- function(x, decreasing = FALSE, ...) {
  numeric_vals <- get_filetype_numeric(x)
  sorted_indices <- order(numeric_vals, decreasing = decreasing, na.last = TRUE)
  x[sorted_indices]
}


#' @include 01_set_generics.R
#' @rdname steps_filetype
#' @export
rank.steps_filetype <- function(x, ties.method = "average") {
  numeric_vals <- get_filetype_numeric(x)
  rank(numeric_vals, ties.method = ties.method, na.last = "keep")
}
