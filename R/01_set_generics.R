#' @export
#' @importFrom methods setGeneric
methods::setGeneric("match")

#' @export
#' @importFrom methods setGeneric
methods::setGeneric("%in%")
