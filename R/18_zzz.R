.onAttach <- function(libname, pkgname) {
  p_path <- get_p_path(error = FALSE)

  packageStartupMessage("analycyte_projects_folder() location:\n", p_path)
}

utils::globalVariables(c(
  "prerequite_type", "output_type", "PID"
))
