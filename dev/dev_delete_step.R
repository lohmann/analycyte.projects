test_delete_step <- function(project, SID, archived = FALSE) {
  p_path <- get_p_path()
  
  projects_path <- make_rds_path("projects", p_path)
  projects_table <- get_rds(projects_path)
  if (!archived) {
    projects_table <- remove_archived(projects_table)
  }
  project_row <-
    validate_unique_entry(project, table = projects_table, what = "project")
  
  steps_path <- make_rds_path("steps", p_path)
  steps_table <- get_rds(steps_path)
  
  SID <- validate_single_integer(SID, na.ok = FALSE, min = 1L, max = Inf)
  
  step_row <-
    validate_unique_entry(
      SID,
      table = steps_table[steps_table$PID == project_row$id, ],
      what = "steps"
    )
  
  path <- paste(project_row$path, step_row$hash_tag, sep = "/")
  
  project_row %>%
    dplyr::select(PID = "id", project = "title") %>%
    dplyr::inner_join(step_row, ., by = "PID") %>%
    print()
  
  
  
  if (fs::dir_exists(path)) {
    if (step_row$hash_tag == "") {
      if (!is.na(step_row$comment) && step_row$comment == "new project") {
        user_prompt(
          msg =
            paste0(
              "\nBeware that the step corresponds to a project creation",
              "\nits path is the following, would you erase the dependancies?:\n\n",
              path,
              "\n\n? (y/n)"
            ),
          n_msg =
            paste0(
              "\nDeletion not completed. If deletion is desired, try ",
              'again and enter "y".'
            )
        )
        # fs::dir_delete(path = path)
        
        steps_table <- steps_table %>%
          # dplyr::anti_join(step_row, by = c("PID", "SID"))%>%
          dplyr::filter(!PID == project) %>%
          dplyr::arrange(.data$PID)
        
        
        delete_project(project = project)
        
        if (!nrow(steps_table)) {
          message("\nNo more steps remaining")
        }
        
        return()
      }
    } else {
      
      # hash_tag is not empty, check hash_tag is in  input_hash_tag columns for other steps
      corresponding_hash <- step_row$hash_tag 
      
      # .... add a feature HERE
      # check if corresponding_hash is in input_hash_tag
      # ask to the user if he is sure he want to delete corresponding_hash that will cause the deletion of 
      # other steps with corresponding_hash dependance (in the input_hash_tag column)
      # if yes then delete the folders of hash_tags for this steps  
      # Find steps that depend on the corresponding_hash
      dependent_steps <- steps_table[which(steps_table$input_hash_tag %in% corresponding_hash), ]
      
      

      
      if (nrow(dependent_steps) > 0) {
        
        
           # maybe print dependent steps 
      
      steps_table %>%
        dplyr::filter(
          hash_tag == corresponding_hash | 
          input_hash_tag == corresponding_hash
          ) %>%
        print()
      
      # > path <- paste(project_row$path, step_row$hash_tag, sep = "/")
      paths <- sapply(dependent_steps$hash_tag, function(x) paste(project_row$path, x, sep = "/"))
      all_paths <- c(path,paths)
        user_prompt(
          msg =
            paste0(
              "\nAre you sure you want to delete the above steps",
              "\nand their folders, which are located at:\n\n",
              paste0(all_paths, collapse = "\n"), 
              "\n\n? (y/n)"
            ),
          n_msg =
            paste0(
              "\nDeletion not completed. If deletion is desired, try ",
              'again and enter "y".'
            )
        )
        
        # fs::dir_delete(path = path)
        #  delete the folders for dependent steps
        for (path in all_paths) {
          if (file.exists(path)) {
            fs::dir_delete(path)
          } else {
            print(paste("File or directory does not exist:", path))
          }
        }
        
        # Remove the step and dependent steps from the steps_table
        step_row <- steps_table[(steps_table$SID %in% c(SID, dependent_steps$SID)), ]
        
      }else{
        
        user_prompt(
        msg =
          paste0(
            "\nAre you sure you want to delete the above step",
            "\nand its entire folder, which is located at\n\n",
            path,
            "\n\n? (y/n)"
          ),
        n_msg =
          paste0(
            "\nDeletion not completed. If deletion is desired, try ",
            'again and enter "y".'
          )
      )
      
      fs::dir_delete(path = path)
        
      }
      
   
    }
  } else {
    user_prompt(
      msg = paste0(
        "Step folder not found at\n", path,
        "\nDelete only its metadata? (y/n)"
      ),
      n_msg = paste0(
        "Deletion not completed. Restore folder to ",
        path, ' or rerun this command, inputting "y" ',
        'instead of "n" when asked whether or not to continue.'
      )
    )
  }
  
  steps_table <- steps_table %>%
    dplyr::anti_join(step_row, by = c("PID", "SID")) %>%
    dplyr::arrange(.data$PID)
  # %>%  sort_project_steps(PID = step_row$PID)
  
  save_metadata(steps_table, steps_path, steps_ptype)
  
  project_steps <-
    project_row %>%
    dplyr::select(PID = "id", project = "title") %>%
    dplyr::inner_join(steps_table, ., by = "PID") %>%
    dplyr::arrange(.data$PID, .data$SID)
  
  if (nrow(project_steps)) {
    message("\nUpdated project step list:")
    project_steps
  } else {
    message("\nNo more steps remaining for project ", project_row$id)
    invisible(project_steps)
  }
}