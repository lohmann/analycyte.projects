heatmap_catalyst_3(
  se = se,
  id = id,
  exprs = "counts",
  se_mfi = se_mfi,
  id_mfi = id_mfi,
  subset_marker = NULL,
  subset_cluster = NULL,
  scaled_MFI = scaled_MFI,
  subset_patient = NULL,
  marker_id = marker_column_id,
  c_id = c_id,
  annotation_ = {
    if (annotation_ != c_id) annotation_ else NULL
  },
  clustr = T,
  clustc = T,
  margin_ = 2,
  t_metadata = t_metadata,
  split_heat = NULL,
  fontsize_ = 8,
  round_ = 8,
  applied_fct = "median",
  log_bar = 0,
  title = "All Samples"
)

heatmap_catalyst_4(
  se = se,
  id = id,
  exprs = "counts",
  se_mfi = se_mfi,
  id_mfi = id_mfi,
  subset_marker = NULL,
  subset_cluster = NULL,
  scaled_MFI = scaled_MFI,
  subset_patient = NULL,
  marker_id = marker_column_id,
  c_id = c_id,
  annotation_ = {
    if (annotation_ != c_id) annotation_ else NULL
  },
  clustr = T,
  clustc = T,
  margin_ = 2,
  t_metadata = t_metadata,
  split_heat = NULL,
  fontsize_ = 8,
  round_ = 8,
  applied_fct = "median",
  log_bar = 0,
  title = "All Samples"
)


heatmap_catalyst_3 <- function(
    se = NULL,
    exprs = "counts",
    id_mfi = "shared_col",
    se_mfi = NULL,
    scaled_MFI = NULL, # NEW
    subset_patient = NULL, # NEW
    marker_id = NULL, # NEW 'desc'
    subset_marker = NULL,
    subset_cluster = NULL,
    clustr = T,
    clustc = T,
    margin_ = 2,
    annotation_ = NULL,
    c_id = NULL,
    # q_ = 0.01,
    fontsize_ = 8,
    round_ = 8,
    t_metadata = "meta",
    applied_fct = "mean",
    split_heat = NULL,
    title = NULL,
    id = NULL,
    log_bar = F) {
  checkmate::assert(!is.null(se))
  checkmate::assert(inherits(se, "SummarizedExperiment"), "se must be a SummarizedExperiment object.")
  checkmate::assert(!is.null(se_mfi))
  checkmate::assert(inherits(se_mfi, "SummarizedExperiment"), "se must be a SummarizedExperiment object.")
  checkmate::assert(!is.null(scaled_MFI))
  checkmate::assert(!is.null(t_metadata))
  checkmate::assert(scaled_MFI %in% names(se_mfi@metadata))
  
  rowdata <- getFeature(se, c("rowData"))

  
  
    
  mfis_table <- getFeature(
    se_object = se_mfi, target_vector = c("metadata", scaled_MFI), round_value = round_
  )
  
  metadata_table <- getFeature(
    se_object = se, target_vector = c("metadata", t_metadata)
  )
  
  counts_table <- getFeature(
    se_object = se, target_vector = c("assays", exprs), round_value = round_
  )
  
  
  if (!is.null(annotation_) && !is.null(c_id) && length(annotation_) > 0 && length(c_id) > 0 && annotation_ != c_id) {
    colnames(mfis_table)[-c(1, 2)] <- rowdata[[annotation_]][match(colnames(mfis_table)[-c(1, 2)], rowdata[[c_id]])]
    
    rownames(counts_table) <- rowdata[[annotation_]][match(rownames(counts_table), rowdata[[c_id]])]
  }
  
  
  
  if (!is.null(subset_patient)) { # is working well
    checkmate::assert_true(all(subset_patient %in% colnames(counts_table)))
    checkmate::assert(!is.null(id_mfi))
    checkmate::assert_true(all(subset_patient %in% c(mfis_table |> pull(!!rlang::sym(id_mfi)) |> unique())))
    
    counts_table <- counts_table[, subset_patient, drop = F]
    mfis_table <- mfis_table |> dplyr::filter(!!rlang::sym(id_mfi) %in% subset_patient)
    
    metadata_table <- metadata_table %>%
      filter(!!rlang::sym(id) %in% subset_patient)
  }
  
  
  
  mfi_matrix(
    mfis_table = mfis_table,
    applied_fct = applied_fct,
    marker_id = marker_id,
    marker_and_file_pos = c(1, 2)
  ) |> t() -> tb_
  
  tb_ <- tb_[, sort(colnames(tb_))]
  
  annot_markers <- getFeature(se_mfi, c("metadata", "annot_markers"))
  
  if(all(c("min value" , "max value")%in%colnames(annot_markers))){
      annot_markers <- t(annot_markers[c(c("min value" , "max value"))]) |> data.frame(check.names = F)

  }else{
      warning('Missing "min value" or "max value" configuration columns, use mfi_min_max() function.')
      mfi_min_max(se_mfi, markers = sort(colnames(tb_))) |> t() |> data.frame(check.names = F) -> annot_markers

  }

  if (!is.null(subset_marker)) {
    tb_ <- tb_[, subset_marker, drop = F]
    annot_markers <- annot_markers[ ,subset_marker, drop = F]
  }
  if (!is.null(subset_cluster)) {
    subset_cluster_ <- subset_cluster[subset_cluster %in% rownames(tb_)]
    tb_ <- tb_[subset_cluster_, , drop = F]
    counts_table <- counts_table[subset_cluster_, , drop = F]
  }
  
  
  if (exprs == "counts") {
    column_ha_tmp <- counts_table |> rowSums()
    percent_column_ha_tmp <- round(column_ha_tmp / sum(column_ha_tmp) * 100, 2)
  }
  
  tb_ <- tb_[match(names(column_ha_tmp), rownames(tb_)), ] # reorder tb_
  
  names_perc <- paste0(names(column_ha_tmp), " (", percent_column_ha_tmp, "%)")
  names(column_ha_tmp) <- names_perc
  rownames(tb_) <- names_perc
  
  anno_title <- ifelse(log_bar, "Count[log]", "Count[x]")
  
  freqbar <- setNames(
    list(
      ComplexHeatmap::anno_barplot(
        as.matrix(column_ha_tmp) %>%
          {
            if (log_bar) log(.) else .
          },
        border = F,
        title = anno_title,
        axis_param = list(gp = gpar(fontsize = fontsize_ - 2)),
        which = "row"
      )
    ),
    anno_title
  )
  
  
  column_ha <- do.call(
    rowAnnotation,
    c(
      freqbar,
      list(
        annotation_name_gp = gpar(fontsize = fontsize_),
        width = unit(20, "mm")
      )
    )
  )
  
  
  tb_[is.na(tb_)] <- 0 # no need to check for na distance i guess
  
  h1 <-     ComplexHeatmap::Heatmap(tb_,
                                    rect_gp = gpar(col = "white"),
                                    col = colorRamp2(
                                      seq(
                                        0, 1,
                                        l = n <- 9
                                      ),
                                      colorRampPalette(c(rev(RColorBrewer::brewer.pal(6, "Blues")), RColorBrewer::brewer.pal(6, "OrRd")))(n)
                                    ),
                                    # col = customColorRamp2(
                                    #   seq(0, 1, length.out = 9),
                                    #   c(rev(RColorBrewer::brewer.pal(6, "Blues")), RColorBrewer::brewer.pal(6, "OrRd")) ),
                                    right_annotation = column_ha,
                                    row_names_gp = gpar(fontsize = fontsize_ + 2),
                                    column_names_gp = gpar(fontsize = fontsize_ + 2),
                                    name = "Marker expression",
                                    heatmap_legend_param = list(
                                      legend_direction = "horizontal",
                                      at = c(0, 0.25, 0.5, 0.75, 1)
                                    ),
                                    cluster_columns = clustc,
                                    cluster_rows = clustr
  )
  
  order_for_h2 <- column_order( draw(h1) )
  
  
  h2 <-   ComplexHeatmap::Heatmap(as.matrix(annot_markers[colnames(tb_)]),
                          column_order = order_for_h2, 
                          rect_gp = gpar(col = "white"),
                          cluster_rows = F,
                          name = "Marker min/max",
                          row_names_side = "left",
                         col =  circlize::colorRamp2(
                            c(min(as.matrix(annot_markers), na.rm = TRUE),  # Minimum value
                              0,  # Median value
                              max(as.matrix(annot_markers), na.rm = TRUE)),  # Maximum value
                            c("blue", "white", "red")  # Colors
                          ),
                          heatmap_legend_param = list(
                            legend_direction = "horizontal",
                             legend_width = unit(30, "mm")
                            )
  )
  
  ht_combined = h1 %v% h2
  
  
  heat <- ComplexHeatmap::draw(
    ht_combined
,
    heatmap_legend_side = "bottom",
    column_title = if (!is.null(title)) {
      title
    }
  )

  
  
}



heatmap_catalyst_4 <- function(
    se = NULL,
    exprs = "counts",
    id_mfi = "shared_col",
    se_mfi = NULL,
    scaled_MFI = NULL, # NEW
    subset_patient = NULL, # NEW
    marker_id = NULL, # NEW 'desc'
    subset_marker = NULL,
    subset_cluster = NULL,
    clustr = T,
    clustc = T,
    margin_ = 2,
    annotation_ = NULL,
    c_id = NULL,
    # q_ = 0.01,
    fontsize_ = 8,
    round_ = 8,
    t_metadata = "meta",
    applied_fct = "mean",
    split_heat = NULL,
    title = NULL,
    id = NULL,
    log_bar = F) {
  checkmate::assert(!is.null(se))
  checkmate::assert(inherits(se, "SummarizedExperiment"), "se must be a SummarizedExperiment object.")
  checkmate::assert(!is.null(se_mfi))
  checkmate::assert(inherits(se_mfi, "SummarizedExperiment"), "se must be a SummarizedExperiment object.")
  checkmate::assert(!is.null(scaled_MFI))
  checkmate::assert(!is.null(t_metadata))
  checkmate::assert(scaled_MFI %in% names(se_mfi@metadata))
  
  rowdata <- getFeature(se, c("rowData"))
  
  mfis_table <- getFeature(
    se_object = se_mfi, target_vector = c("metadata", scaled_MFI), round_value = round_
  )
  
  metadata_table <- getFeature(
    se_object = se, target_vector = c("metadata", t_metadata)
  )
  
  counts_table <- getFeature(
    se_object = se, target_vector = c("assays", exprs), round_value = round_
  )
  
  
  if (!is.null(annotation_) && !is.null(c_id) && length(annotation_) > 0 && length(c_id) > 0 && annotation_ != c_id) {
    colnames(mfis_table)[-c(1, 2)] <- rowdata[[annotation_]][match(colnames(mfis_table)[-c(1, 2)], rowdata[[c_id]])]
    
    rownames(counts_table) <- rowdata[[annotation_]][match(rownames(counts_table), rowdata[[c_id]])]
  }
  
  
  
  if (!is.null(subset_patient)) { # is working well
    checkmate::assert_true(all(subset_patient %in% colnames(counts_table)))
    checkmate::assert(!is.null(id_mfi))
    checkmate::assert_true(all(subset_patient %in% c(mfis_table |> pull(!!rlang::sym(id_mfi)) |> unique())))
    
    counts_table <- counts_table[, subset_patient, drop = F]
    mfis_table <- mfis_table |> dplyr::filter(!!rlang::sym(id_mfi) %in% subset_patient)
    
    metadata_table <- metadata_table %>%
      filter(!!rlang::sym(id) %in% subset_patient)
  }
  
  annot_markers <- getFeature(se_mfi, c("metadata", "annot_markers"))
  
  if(all(c("min value" , "max value")%in%colnames(annot_markers))){
    annot_markers <- annot_markers[c(c("min value" , "max value"))] |> t() |> data.frame(check.names = F)
    
  }else{
    warning('Missing "min value" or "max value" configuration columns, use mfi_min_max() function.')
    mfi_min_max(se_mfi, markers=NULL) |> t() |> data.frame(check.names = F) -> annot_markers
    
  }
  
  mfi_matrix_2(
    mfis_table = mfis_table,
    applied_fct = applied_fct,
    marker_id = marker_id,
    marker_and_file_pos = c(1, 2)
  ) -> heatmap_merged_mfis
  
  
  # then other function to scale, with min max 
  #
  #
  #
  scale_mfi_0_to_1(
    heat_demo = heatmap_merged_mfis,
    min_max_markers = t(annot_markers)
    ) |> t()  -> tb_
  
  
  
  tb_ <- tb_[, sort(colnames(tb_))]
  

  
  if (!is.null(subset_marker)) {
    tb_ <- tb_[, subset_marker, drop = F]
    annot_markers <- annot_markers[ ,subset_marker, drop = F]
  }
  if (!is.null(subset_cluster)) {
    subset_cluster_ <- subset_cluster[subset_cluster %in% rownames(tb_)]
    tb_ <- tb_[subset_cluster_, , drop = F]
    counts_table <- counts_table[subset_cluster_, , drop = F]
  }
  
  
  if (exprs == "counts") {
    column_ha_tmp <- counts_table |> rowSums()
    percent_column_ha_tmp <- round(column_ha_tmp / sum(column_ha_tmp) * 100, 2)
  }
  
  tb_ <- tb_[match(names(column_ha_tmp), rownames(tb_)), ] # reorder tb_
  
  names_perc <- paste0(names(column_ha_tmp), " (", percent_column_ha_tmp, "%)")
  names(column_ha_tmp) <- names_perc
  rownames(tb_) <- names_perc
  
  anno_title <- ifelse(log_bar, "Count[log]", "Count[x]")
  
  freqbar <- setNames(
    list(
      ComplexHeatmap::anno_barplot(
        as.matrix(column_ha_tmp) %>%
          {
            if (log_bar) log(.) else .
          },
        border = F,
        title = anno_title,
        axis_param = list(gp = gpar(fontsize = fontsize_ - 2)),
        which = "row"
      )
    ),
    anno_title
  )
  
  
  column_ha <- do.call(
    rowAnnotation,
    c(
      freqbar,
      list(
        annotation_name_gp = gpar(fontsize = fontsize_),
        width = unit(20, "mm")
      )
    )
  )
  
  
  tb_[is.na(tb_)] <- 0 # no need to check for na distance i guess
  
  h1 <-     ComplexHeatmap::Heatmap(tb_,
                                    rect_gp = gpar(col = "white"),
                                    col = colorRamp2(
                                      seq(
                                        0, 1,
                                        l = n <- 9
                                      ),
                                      colorRampPalette(c(rev(RColorBrewer::brewer.pal(6, "Blues")), RColorBrewer::brewer.pal(6, "OrRd")))(n)
                                    ),
                                    # col = customColorRamp2(
                                    #   seq(0, 1, length.out = 9),
                                    #   c(rev(RColorBrewer::brewer.pal(6, "Blues")), RColorBrewer::brewer.pal(6, "OrRd")) ),
                                    right_annotation = column_ha,
                                    row_names_gp = gpar(fontsize = fontsize_ + 2),
                                    column_names_gp = gpar(fontsize = fontsize_ + 2),
                                    name = "Marker expression",
                                    heatmap_legend_param = list(
                                      legend_direction = "horizontal",
                                      at = c(0, 0.25, 0.5, 0.75, 1)
                                    ),
                                    cluster_columns = clustc,
                                    cluster_rows = clustr
  )
  
  order_for_h2 <- column_order(prepare(h1))
  
  
  h2 <-   ComplexHeatmap::Heatmap(as.matrix(annot_markers[colnames(tb_)]),
                                  column_order = order_for_h2, 
                                  rect_gp = gpar(col = "white"),
                                  cluster_rows = F,
                                  name = "Marker min/max",
                                  row_names_side = "left",
                                  col =  circlize::colorRamp2(
                                    c(min(as.matrix(annot_markers), na.rm = TRUE),  # Minimum value
                                      0,  # Median value
                                      max(as.matrix(annot_markers), na.rm = TRUE)),  # Maximum value
                                    c("blue", "white", "red")  # Colors
                                  ),
                                  heatmap_legend_param = list(
                                    legend_direction = "horizontal",
                                    legend_width = unit(30, "mm")
                                  )
  )
  
  ht_combined = h1 %v% h2
  
  
  heat <- ComplexHeatmap::draw(
    ht_combined
    ,
    heatmap_legend_side = "bottom",
    column_title = if (!is.null(title)) {
      title
    }
  )
  
  
  
}
